FROM ubuntu:latest
ENV TZ=Asia/Dubai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get -y update
RUN apt-get -y install apache2
COPY ./index.html /var/www/html
EXPOSE 80
CMD apachectl -D FOREGROUND
